import 'package:flutter/material.dart';
import 'package:lesson7/app/ui/persons_list/widgets/person_grid_tile.dart';
import 'package:lesson7/app/ui/persons_list/widgets/person_list_tile.dart';
import 'package:lesson7/app/ui/persons_list/widgets/search_field.dart';
import 'package:lesson7/app/ui/persons_list/widgets/vmodel.dart';
import 'package:provider/provider.dart';

import '../../../generated/l10n.dart';
import '../../db/repo_persons.dart';
import '../../models/person.dart';
import '../../settings/constants/app_colors.dart';
import '../../settings/constants/app_styles.dart';
import '../../widgets/app_nav_bar.dart';

part 'widgets/greed_view.dart';
part 'widgets/list_view.dart';

class PersonsList extends StatelessWidget {
  const PersonsList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: const AppNavBar(current: 0),
        body: ChangeNotifierProvider(
          create: (context) => PersonsListVModel(
            repo: Provider.of<RepoPersons>(context, listen: false),
          ),
          builder: (context, _) {
            final personsTotal =
                context.watch<PersonsListVModel>().filteredList.length;
            return Column(
              children: [
                SearchField(
                  onChanged: (value) {
                    Provider.of<PersonsListVModel>(context, listen: false)
                        .filter(
                      value.toLowerCase(),
                    );
                  },
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: Text(
                          S
                              .of(context)
                              .personsTotal(personsTotal)
                              .toUpperCase(),
                          style: AppStyles.s10w500.copyWith(
                            letterSpacing: 1.5,
                            color: AppColors.neutral2,
                          ),
                        ),
                      ),
                      IconButton(
                        icon: const Icon(Icons.grid_view),
                        iconSize: 28.0,
                        color: AppColors.neutral2,
                        onPressed: () {
                          Provider.of<PersonsListVModel>(
                            context,
                            listen: false,
                          ).switchView();
                        },
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Consumer<PersonsListVModel>(
                    builder: (context, vmodel, _) {
                      if (vmodel.isLoading) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            CircularProgressIndicator(),
                          ],
                        );
                      }
                      if (vmodel.errorMessage != null) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Flexible(
                              child: Text(vmodel.errorMessage!),
                            ),
                          ],
                        );
                      }
                      if (vmodel.filteredList.isEmpty) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Flexible(
                              child: Text(S.of(context).personsListIsEmpty),
                            ),
                          ],
                        );
                      }
                      return vmodel.isListView
                          ? _ListView(
                              personsList: vmodel.filteredList,
                            )
                          : _GridView(
                              personsList: vmodel.filteredList,
                            );
                    },
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
