import '../models/person.dart';

class ConnectDB {
  static List<Person> all() {
    return [
      Person(
        name: 'Рик Санчез',
        species: 'Человек',
        status: 'Alive',
        gender: 'Мужской',
      ),
      Person(
        name: 'Алан Райс',
        species: 'Человек',
        status: 'Dead',
        gender: 'Мужской',
      ),
      Person(
        name: 'Саммер Смит',
        species: 'Человек',
        status: 'Alive',
        gender: 'Женский',
      ),
      Person(
        name: 'Морти Смит',
        species: 'Человек',
        status: 'Alive',
        gender: 'Мужской',
      ),
    ];
  }
}
